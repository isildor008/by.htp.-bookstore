package by.htp.book.store.commad;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Dmitry on 19.10.2017.
 */
public interface CommandAction {
    String execute(HttpServletRequest request, HttpServletResponse response);
}
