package by.htp.book.store.commad;

import by.htp.book.store.commad.commandImpl.GetLoginPage;
import by.htp.book.store.commad.commandImpl.GetMainPageCommand;
import by.htp.book.store.commad.commandImpl.LogOutAction;
import by.htp.book.store.commad.commandImpl.LoginAction;
import by.htp.book.store.commad.commandImpl.admin.GetAllUserCommand;

/**
 * Created by Dmitry on 10.11.2017.
 */
public class CommandChoser {

    public static CommandAction getCurrentCommand(String action) {
        switch (action) {
            case "start":
                return new GetMainPageCommand();
            case "admin":
                return new GetAllUserCommand();
            case "login":
                return new GetLoginPage();
            case "setUserDates":
                return new LoginAction();
            case "logout" :
                return new LogOutAction();
            default:
                return null;}
    }
}
