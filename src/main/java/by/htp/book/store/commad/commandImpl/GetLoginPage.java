package by.htp.book.store.commad.commandImpl;

import by.htp.book.store.commad.CommandAction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Dmitry on 16.11.2017.
 */
public class GetLoginPage implements CommandAction {
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        return "WEB-INF/page/loginPage.jsp";
    }
}
