package by.htp.book.store.commad.commandImpl;

import by.htp.book.store.commad.CommandAction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by Dmitry on 21.11.2017.
 */
public class LogOutAction implements CommandAction {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        session.invalidate();
        return "WEB-INF/page/index.jsp";
    }
}
