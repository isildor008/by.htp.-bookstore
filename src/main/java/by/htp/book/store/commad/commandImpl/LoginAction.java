package by.htp.book.store.commad.commandImpl;

import by.htp.book.store.commad.CommandAction;
import by.htp.book.store.entity.User;
import by.htp.book.store.exception.DaoException;
import by.htp.book.store.service.UserService;
import by.htp.book.store.service.impl.UserServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by Dmitry on 16.11.2017.
 */
public class LoginAction implements CommandAction {

    private static final Logger log = Logger.getLogger(LoginAction.class);

    private UserService userService;

    public LoginAction() {
        userService = new UserServiceImpl();
    }

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String page = null;
        User user;
        try {
            user = userService.getUserByLoinAndPass(login, password);
            if (user.getLogin() == null) {
                request.setAttribute("firstName", "User not register");
                page = "WEB-INF/page/index.jsp";

            } else {
                HttpSession session = request.getSession();
                session.setAttribute("firstName", user.getFirstName());
                page = "WEB-INF/page/index.jsp";
            }
        } catch (DaoException e) {
            log.error(e + "Problem with page login action");
        }
        return page;
    }
}
