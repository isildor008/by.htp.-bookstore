package by.htp.book.store.controller;

import by.htp.book.store.commad.CommandAction;
import by.htp.book.store.commad.CommandChoser;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Dmitry on 10.10.2017.
 */
public class MainServlet extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");

        String page = null;
        if (action != null) {
            CommandAction currentAction = CommandChoser.getCurrentCommand(action);
            page = currentAction.execute(request, response);

            RequestDispatcher disp = request.getRequestDispatcher(page);
            disp.forward(request, response);
         //   response.sendRedirect(request.getContextPath() + page);
        }
    }
}
