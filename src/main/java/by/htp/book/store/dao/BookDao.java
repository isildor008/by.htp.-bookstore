package by.htp.book.store.dao;

import by.htp.book.store.entity.Book;
import by.htp.book.store.exception.DaoException;


import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.util.List;

/**
 * Created by Dmitry on 19.10.2017.
 */
public interface BookDao {

    void addNewBook(String title, String nameAuthor, int date, int countPage, int countSample, int status, BigDecimal price) throws DaoException;

    void editBook(String title, String nameAuthor, int date, int countPage, int countSample, int status, BigDecimal price, int id) throws DaoException;

    List<Book> getBookList() throws DaoException;

    void delete(int id) throws DaoException;

    List<Book> getBookListByStatus(int status) throws DaoException;

    List<Book> getSortedListBooks(String order,String sortingRule) throws DaoException;

    void deleteTestedBook();

    Book getBookByCountPage(Book book) throws DaoException;

}
