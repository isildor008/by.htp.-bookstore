package by.htp.book.store.dao;

import by.htp.book.store.entity.Order;
import by.htp.book.store.exception.DaoException;

import java.sql.Timestamp;

/**
 * Created by Dmitry on 19.10.2017.
 */
public interface OrderDao {

    void saveOrder(int userId, Timestamp date, int status) throws DaoException;
    Order getByStatus(int status);
    void deleteTestedOrder();
}
