package by.htp.book.store.dao;

import by.htp.book.store.exception.DaoException;
import by.htp.book.store.model.BookModel;
import by.htp.book.store.model.OrderModel;

import java.util.List;

/**
 * Created by Dmitry on 31.10.2017.
 */
public interface OrderModelDao {

    List<OrderModel> getAllOrderModel() throws DaoException;
    List<BookModel> getAllBookByOrder(int id) throws DaoException ;
}
