package by.htp.book.store.dao;

import by.htp.book.store.entity.SubOrder;
import by.htp.book.store.exception.DaoException;

import java.util.List;

/**
 * Created by Dmitry on 03.11.2017.
 */
public interface SubOrderDao {

    void saveSubOrder(int orderId, int bookId) throws DaoException;

    SubOrder getOrderById(int id);

    void deleteSubOrderByBookId(int bookId);

    List<SubOrder> getListByOrderId(int id);
}
