package by.htp.book.store.dao;

import by.htp.book.store.entity.User;
import by.htp.book.store.exception.DaoException;

import java.util.List;

/**
 * Created by Dmitry on 19.10.2017.
 */
public interface UserDao {

    void addNewUser(String login, String pass, String firstName, String lastName, String email, int status, int role) throws DaoException;

    void editUser(String log, String pass, String firstName, String lastName, String email, int status, int role, int userId) throws DaoException;

    List<User> getUserList() throws DaoException;

    void delete(int id) throws DaoException;

    List<User> getUserListByStatus(int status) throws DaoException;

    List<User> getSortedListUsers(String sortingRule, String order) throws DaoException;

    User getUserById(int id) throws DaoException;

    void deleteUserByStatus(int status);

    User getUserByLoginAndPass(String login, String pass) throws DaoException;
}
