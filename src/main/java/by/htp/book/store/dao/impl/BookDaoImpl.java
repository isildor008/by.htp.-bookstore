package by.htp.book.store.dao.impl;

import by.htp.book.store.dao.BookDao;
import by.htp.book.store.entity.Book;
import by.htp.book.store.exception.DaoException;
import by.htp.book.store.util.ConnectionPool;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.sql.*;

import java.util.*;

import static by.htp.book.store.util.ExceptionName.*;
import static by.htp.book.store.util.SQLConstant.*;


/**
 * Created by Dmitry on 19.10.2017.
 */
public class BookDaoImpl implements BookDao {

    private static final Logger log = Logger.getLogger(BookDaoImpl.class);


    public void addNewBook(String title, String nameAuthor, int date, int countPage, int countSample, int status, BigDecimal price) throws DaoException {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = pool.take();
            preparedStatement = connection.prepareStatement(INSERT_BOOK);
            preparedStatement.setString(1, title);
            preparedStatement.setString(2, nameAuthor);
            preparedStatement.setInt(3, date);
            preparedStatement.setInt(4, countPage);
            preparedStatement.setInt(5, countSample);
            preparedStatement.setInt(6, status);
            preparedStatement.setBigDecimal(7, price);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            log.error(EXCEPTION_ADD_NEW_BOOK_IN_DB, e);
            throw new DaoException(EXCEPTION_ADD_NEW_BOOK_IN_DB);

        } finally {
            pool.closeConnection(connection, preparedStatement);
        }
    }


    public void editBook(String title, String nameAuthor, int date, int countPage, int countSample, int status, BigDecimal price, int id) throws DaoException {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = pool.take();
            preparedStatement = connection.prepareStatement(UPDATE_BOOK);
            preparedStatement.setString(1, title);
            preparedStatement.setString(2, nameAuthor);
            preparedStatement.setInt(3, date);
            preparedStatement.setInt(4, countPage);
            preparedStatement.setInt(5, countSample);
            preparedStatement.setInt(6, status);
            preparedStatement.setBigDecimal(7, price);
            preparedStatement.setInt(8, id);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            log.error(EXCEPTION_EDIT_BOOK, e);
            throw new DaoException(EXCEPTION_EDIT_BOOK);

        } finally {
            pool.closeConnection(connection, preparedStatement);
        }
    }


    public List<Book> getBookList() throws DaoException {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection;
        PreparedStatement preparedStatement = null;
        connection = pool.take();

        ResultSet resultSet;
        List<Book> listBook = new ArrayList<Book>();
        try {
            preparedStatement = connection.prepareStatement(GET_ALL_BOOK);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                listBook.add(createBook(resultSet, new Book()));
            }
        } catch (SQLException e) {
            log.error(EXCEPTION_GET_ALL_BOOK, e);
            throw new DaoException(EXCEPTION_GET_ALL_BOOK);
        } finally {
            pool.closeConnection(connection, preparedStatement);
        }


        return listBook;
    }

    private Book createBook(ResultSet resultSet, Book book) throws SQLException {
        book.setId(resultSet.getInt("id"));
        book.setTitle(resultSet.getString("title"));
        book.setNameAuthor(resultSet.getString("name_author"));
        book.setDate(resultSet.getInt("date"));
        book.setCountPage(resultSet.getInt("count_page"));
        book.setCountSample(resultSet.getInt("count_sample"));
        book.setStatus(resultSet.getInt("status"));
        book.setPrice(resultSet.getBigDecimal("price"));
        return book;
    }

    public void delete(int id) throws DaoException {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection;
        PreparedStatement preparedStatement = null;
        connection = pool.take();

        try {
            preparedStatement = connection.prepareStatement(DELETE_BOOK);
            preparedStatement.setInt(1, NOT_AVAILABLE_BOOK);
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            log.error(EXCETPION_DELETE_BOOK, e);
            throw new DaoException(EXCETPION_DELETE_BOOK);

        } finally {
            pool.closeConnection(connection, preparedStatement);
        }
    }

    public List<Book> getBookListByStatus(int status) throws DaoException {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection=null;
        PreparedStatement preparedStatement = null;


        ResultSet resultSet;
        List<Book> listBook = new ArrayList<Book>();
        try {
            connection = pool.take();
            preparedStatement = connection.prepareStatement(GET_BOOK_LIST_BY_STATUS);
            preparedStatement.setInt(1, status);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {

                listBook.add(createBook(resultSet, new Book()));
            }
        } catch (SQLException e) {
            log.error(EXCETPION_GET_BOOK_BY_STATUS, e);
            throw new DaoException(EXCETPION_GET_BOOK_BY_STATUS, e);
        } finally {
            pool.closeConnection(connection, preparedStatement);
        }


        return listBook;
    }

    public List<Book> getSortedListBooks(String sortingRule, String order) throws DaoException {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection=null;
        PreparedStatement preparedStatement = null;

        ResultSet resultSet;
        List<Book> listBook = new ArrayList<Book>();
        try {
            connection = pool.take();
            preparedStatement = connection.prepareStatement(sortingRule + order);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                listBook.add(createBook(resultSet, new Book()));
            }
        } catch (SQLException e) {
            log.error(EXCETPION_GET_SORTED_LIST_BOOK, e);
            throw new DaoException(EXCETPION_GET_SORTED_LIST_BOOK);
        } finally {
            pool.closeConnection(connection, preparedStatement);
        }
        return listBook;
    }

    //only for tests

    public void deleteTestedBook(){
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection;
        PreparedStatement preparedStatement=null;
        connection = pool.take();
        try {
            preparedStatement = connection.prepareStatement(DELETE_BOOK_BY_COUNT_PAGE);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            pool.closeConnection(connection, preparedStatement);
        }
    }

    public Book getBookByCountPage(Book book) throws DaoException {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection=null;
        PreparedStatement preparedStatement=null;
        ResultSet resultSet;
        try {
            connection = pool.take();
            preparedStatement = connection.prepareStatement(GET_BOOK_BY_COUNT_PAGE);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                createBook(resultSet, book);
            }
        } catch (SQLException e) {
            log.error(EXCETPION_GET_SORTED_LIST_BOOK, e);
            throw new DaoException(EXCEPTION_GET_GET_BOOK_BY_COUNT_PAGE);
        }finally {
            pool.closeConnection(connection, preparedStatement);
        }return book;
    }


}
