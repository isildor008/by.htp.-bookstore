package by.htp.book.store.dao.impl;

import by.htp.book.store.dao.OrderDao;
import by.htp.book.store.entity.Order;
import by.htp.book.store.exception.DaoException;
import by.htp.book.store.util.ConnectionPool;
import org.apache.log4j.Logger;

import java.sql.*;

import static by.htp.book.store.util.ExceptionName.EXCEPTION_SAVE_ORDER;
import static by.htp.book.store.util.SQLConstant.DELETE_TESTED_ORDER;
import static by.htp.book.store.util.SQLConstant.GET_ORDER_BY_STATUS;
import static by.htp.book.store.util.SQLConstant.SAVE_ORDER;

/**
 * Created by Dmitry on 19.10.2017.
 */
public class OrderDaoImpl implements OrderDao {

    private static final Logger log = Logger.getLogger(OrderDaoImpl.class);

    public void saveOrder(int userId, Timestamp date, int status) throws DaoException {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = pool.take();
            preparedStatement = connection.prepareStatement(SAVE_ORDER);
            preparedStatement.setInt(1, userId);
            preparedStatement.setTimestamp(2, date);
            preparedStatement.setInt(3, status);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            log.error(EXCEPTION_SAVE_ORDER, e);
            throw new DaoException(EXCEPTION_SAVE_ORDER);

        } finally {
            pool.closeConnection(connection, preparedStatement);
        }

    }

    public Order getByStatus(int status){
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        Order order=new Order();
        connection = pool.take();
        try {
            preparedStatement = connection.prepareStatement(GET_ORDER_BY_STATUS);
            preparedStatement.setInt(1, status);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                createOrder(resultSet, order);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }return order;

    }

    private Order createOrder(ResultSet resultSet, Order order) throws SQLException {
        order.setId(resultSet.getInt("id"));
        order.setDate(resultSet.getTimestamp("date"));
        order.setStatus(resultSet.getInt("status"));
        order.setUserId(resultSet.getInt("user_id"));
        return order;
    }

    public void deleteTestedOrder(){
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection;
        PreparedStatement preparedStatement=null;
        connection = pool.take();
        try {
            preparedStatement = connection.prepareStatement(DELETE_TESTED_ORDER);
            preparedStatement.setInt(1,8);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            pool.closeConnection(connection, preparedStatement);
        }
    }
}
