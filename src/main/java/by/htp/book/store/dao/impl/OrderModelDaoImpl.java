package by.htp.book.store.dao.impl;

import by.htp.book.store.dao.OrderModelDao;
import by.htp.book.store.exception.DaoException;
import by.htp.book.store.model.BookModel;
import by.htp.book.store.model.OrderModel;
import by.htp.book.store.util.ConnectionPool;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static by.htp.book.store.util.ExceptionName.EXCEPTION_CREATE_ORDER_MODEL;
import static by.htp.book.store.util.ExceptionName.EXCEPTION_GET_ALL_BOOK_BY_ORDER;
import static by.htp.book.store.util.ExceptionName.EXCEPTION_GET_ALL_ORDER_MODEL;
import static by.htp.book.store.util.SQLConstant.*;

/**
 * Created by Dmitry on 31.10.2017.
 */
public class OrderModelDaoImpl implements OrderModelDao {

    private static final Logger log = Logger.getLogger(OrderModelDaoImpl.class);

    public List<OrderModel> getAllOrderModel() throws DaoException {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection=null;
        PreparedStatement preparedStatement = null;

        ResultSet resultSet;
        List<OrderModel> listOrderModel = new ArrayList<OrderModel>();
        try {
            connection = pool.take();
            preparedStatement = connection.prepareStatement(GET_ALL_ORDER_TO_MODEL_WHITSOUT_SUM);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                listOrderModel.add(createOrderModel(resultSet, new OrderModel()));
            }
        } catch (SQLException e) {
            log.error(EXCEPTION_GET_ALL_ORDER_MODEL, e);
            throw new DaoException(EXCEPTION_GET_ALL_ORDER_MODEL);
        } finally {
            pool.closeConnection(connection, preparedStatement);
        }
        return listOrderModel;
    }

    private OrderModel createOrderModel(ResultSet resultSet, OrderModel orderModel) throws SQLException, DaoException {

        orderModel.setOrderId(resultSet.getInt("id"));
        orderModel.setUserId(resultSet.getInt("user_id"));
        orderModel.setDate(resultSet.getTimestamp("date"));
        orderModel.setLoginUser(resultSet.getString("login"));

        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection=null;
        PreparedStatement preparedStatement = null;

        try {
            connection = pool.take();
            preparedStatement = connection.prepareStatement(GET_SUM_BY_ORDER);
            preparedStatement.setInt(1, orderModel.getOrderId());
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                orderModel.setPrice(rs.getBigDecimal(1));
            }

        } catch (SQLException e) {
            log.error(EXCEPTION_CREATE_ORDER_MODEL, e);
            throw new DaoException(EXCEPTION_CREATE_ORDER_MODEL);
        } finally {
            pool.closeConnection(connection, preparedStatement);
        }
        return orderModel;
    }

    public List<BookModel> getAllBookByOrder(int id) throws DaoException {

        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection=null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        List<BookModel> listBookModel = new ArrayList<BookModel>();

        try {
            connection = pool.take();
            preparedStatement = connection.prepareStatement(GET_ALL_BOOK_FOR_ORDER);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                BookModel bookModel = new BookModel();
                bookModel.setId(resultSet.getInt("book_id"));
                bookModel.setTitle(resultSet.getString("title"));
                bookModel.setPrice(resultSet.getBigDecimal("price"));
                listBookModel.add(bookModel);
            }
        } catch (SQLException e) {
            log.error(EXCEPTION_GET_ALL_BOOK_BY_ORDER, e);
            throw new DaoException(EXCEPTION_GET_ALL_BOOK_BY_ORDER);
        } finally {
            pool.closeConnection(connection, preparedStatement);
        }
        return listBookModel;
    }
}
