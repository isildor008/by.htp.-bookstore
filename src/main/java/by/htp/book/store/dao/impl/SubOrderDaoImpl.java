package by.htp.book.store.dao.impl;

import by.htp.book.store.dao.SubOrderDao;
import by.htp.book.store.entity.SubOrder;
import by.htp.book.store.exception.DaoException;
import by.htp.book.store.util.ConnectionPool;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static by.htp.book.store.util.ExceptionName.EXCEPTION_ADD_NEW_SUB_ORDER;
import static by.htp.book.store.util.SQLConstant.*;


/**
 * Created by Dmitry on 03.11.2017.
 */
public class SubOrderDaoImpl implements SubOrderDao {

    private static final Logger log = Logger.getLogger(SubOrderDaoImpl.class);

    public void saveSubOrder(int orderId, int bookId) throws DaoException {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = pool.take();
            preparedStatement = connection.prepareStatement(SAVE_SUB_ORDER);
            preparedStatement.setInt(1, orderId);
            preparedStatement.setInt(2, bookId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error(EXCEPTION_ADD_NEW_SUB_ORDER, e);
            throw new DaoException(EXCEPTION_ADD_NEW_SUB_ORDER);
        } finally {
            pool.closeConnection(connection, preparedStatement);
        }
    }

    public SubOrder getOrderById(int id){
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        SubOrder subOrder=new SubOrder();
        try {
            connection = pool.take();
            preparedStatement= connection.prepareStatement(GET_SUB_ORDER_BY_ID);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                createSubOrder(resultSet, subOrder);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            pool.closeConnection(connection, preparedStatement);
        }return subOrder;
    }

    public List<SubOrder> getListByOrderId(int id){
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        List<SubOrder> list=new ArrayList<SubOrder>();

        try {
            connection = pool.take();
            preparedStatement= connection.prepareStatement(GET_SUB_ORDER_LIST_BY_ORDER_ID);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                list.add(createSubOrder(resultSet, new SubOrder()));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            pool.closeConnection(connection, preparedStatement);}
            return list;
    }

    //only for test

    public void deleteSubOrderByBookId(int bookId){
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = pool.take();
            preparedStatement=connection.prepareStatement(DELETE_SUB_ORDER_BY_BOOK_ID);
            preparedStatement.setInt(1, bookId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            pool.closeConnection(connection, preparedStatement);
        }

    }

    private SubOrder createSubOrder(ResultSet resultSet, SubOrder subOrder) throws SQLException {
        subOrder.setId(resultSet.getInt("id"));
        subOrder.setBookId(resultSet.getInt("book_id"));
        subOrder.setOrderId(resultSet.getInt("order_id"));
        return subOrder;
    }


}
