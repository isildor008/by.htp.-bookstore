package by.htp.book.store.dao.impl;

import by.htp.book.store.dao.UserDao;
import by.htp.book.store.entity.User;
import by.htp.book.store.exception.DaoException;
import by.htp.book.store.util.ConnectionPool;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static by.htp.book.store.util.ExceptionName.*;
import static by.htp.book.store.util.SQLConstant.*;

/**
 * Created by Dmitry on 19.10.2017.
 */
public class UserDaoImpl implements UserDao {

    private static final Logger log = Logger.getLogger(UserDaoImpl.class);

    public void addNewUser(String login, String pass, String firstName, String lastName, String email, int status, int role) throws DaoException {

        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = pool.take();
            preparedStatement = connection.prepareStatement(INSERT_USER);
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, pass);
            preparedStatement.setString(3, firstName);
            preparedStatement.setString(4, lastName);
            preparedStatement.setString(5, email);
            preparedStatement.setInt(6, status);
            preparedStatement.setInt(7, role);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error(EXCEPTION_ADD_NEW_USER, e);
            throw new DaoException(EXCEPTION_ADD_NEW_USER);
        } finally {
            pool.closeConnection(connection, preparedStatement);
        }
    }


    public void editUser(String login, String pass, String firstName, String lastName, String email, int status, int role, int userId) throws DaoException {

        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection;
        PreparedStatement preparedStatement = null;
        connection = pool.take();
        try {
            connection = pool.take();
            preparedStatement = connection.prepareStatement(UPDATE_USER);
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, pass);
            preparedStatement.setString(3, firstName);
            preparedStatement.setString(4, lastName);
            preparedStatement.setString(5, email);
            preparedStatement.setInt(6, status);
            preparedStatement.setInt(7, role);
            preparedStatement.setInt(8, userId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error(EXCEPTION_EDIT_USER, e);
            throw new DaoException(EXCEPTION_EDIT_USER);
        } finally {
            pool.closeConnection(connection, preparedStatement);
        }
    }

    public List<User> getUserList() throws DaoException {

        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection;
        PreparedStatement preparedStatement = null;
        connection = pool.take();
        ResultSet resultSet;
        List<User> listUser = new ArrayList<User>();
        try {
            preparedStatement = connection.prepareStatement(GET_ALL_USER);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                listUser.add(createUser(resultSet, new User()));
            }
        } catch (SQLException e) {
            log.error(EXCEPTION_GET_USER_LIST, e);
            throw new DaoException(EXCEPTION_GET_USER_LIST);
        } finally {
            pool.closeConnection(connection, preparedStatement);
        }
        return listUser;
    }

    private User createUser(ResultSet resultSet, User user) throws SQLException {
        user.setId(resultSet.getInt("id"));
        user.setLogin(resultSet.getString("login"));
        user.setPass(resultSet.getString("pass"));
        user.setFirstName(resultSet.getString("first_name"));
        user.setLastName(resultSet.getString("last_name"));
        user.setEmail(resultSet.getString("email"));
        user.setStatus(resultSet.getInt("status"));
        user.setRole(resultSet.getInt("role"));
        return user;
    }

    public void delete(int id) throws DaoException {

        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection;
        PreparedStatement preparedStatement = null;
        connection = pool.take();
        try {
            preparedStatement = connection.prepareStatement(DELETE_USER);
            preparedStatement.setInt(1, INACTIV_USER);
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error(EXCEPTION_DELETE_USER, e);
            throw new DaoException(EXCEPTION_DELETE_USER);
        } finally {
            pool.closeConnection(connection, preparedStatement);
        }
    }

    public List<User> getUserListByStatus(int status) throws DaoException {

        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection;
        PreparedStatement preparedStatement = null;
        connection = pool.take();
        ResultSet resultSet;
        List<User> listUser = new ArrayList<User>();
        try {
            preparedStatement = connection.prepareStatement(GET_USER_LIST_BY_STATUS);
            preparedStatement.setInt(1, status);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                listUser.add(createUser(resultSet, new User()));
            }

        } catch (SQLException e) {
            log.error(EXCEPTION_GET_USERS_BY_STATUS, e);
            throw new DaoException(EXCEPTION_GET_USERS_BY_STATUS);
        } finally {
            pool.closeConnection(connection, preparedStatement);
        }
        return listUser;
    }

    public List<User> getSortedListUsers(String sortingRule, String order) throws DaoException {

        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection;
        PreparedStatement preparedStatement = null;
        connection = pool.take();
        ResultSet resultSet;
        List<User> listUser = new ArrayList<User>();
        try {
            preparedStatement = connection.prepareStatement(sortingRule + order);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                listUser.add(createUser(resultSet, new User()));
            }
        } catch (SQLException e) {
            log.error(EXCEPTION_GET_USERS_BY_SORTING, e);
            throw new DaoException(EXCEPTION_GET_USERS_BY_SORTING);
        } finally {
            pool.closeConnection(connection, preparedStatement);
        }
        return listUser;
    }

    public User getUserById(int id) throws DaoException {

        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection;
        PreparedStatement preparedStatement = null;
        connection = pool.take();
        ResultSet resultSet;
        User user = new User();
        try {
            preparedStatement = connection.prepareStatement(GET_USER_BY_ID);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                createUser(resultSet, user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            log.error(EXCEPTION_GET_USER_BY_ID, e);
            throw new DaoException(EXCEPTION_GET_USER_BY_ID);
        } finally {
            pool.closeConnection(connection, preparedStatement);
        }
        return user;
    }

//only for test
    public void deleteUserByStatus(int status){
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection;
        PreparedStatement preparedStatement = null;
        connection = pool.take();

        try {
            preparedStatement = connection.prepareStatement(DELETE_USER_BY_STATUS);
            preparedStatement.setInt(1, status);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }   finally {
        pool.closeConnection(connection, preparedStatement);
    }

    }

    public User getUserByLoginAndPass(String login, String pass) throws DaoException {

        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection;
        PreparedStatement preparedStatement = null;
        connection = pool.take();
        ResultSet resultSet;
        User user = new User();
        try {
            preparedStatement = connection.prepareStatement(GET_USER_BY_LOGIN_AND_PASS);
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, pass);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                createUser(resultSet, user);
            }
        } catch (SQLException e) {
            log.error(EXCEPTION_GET_USER_BY_ID, e);
            throw new DaoException(EXCEPTION_GET_USER_BY_ID);
        } finally {
            pool.closeConnection(connection, preparedStatement);
        }
        return user;
    }
}
