package by.htp.book.store.entity;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * Created by Dmitry on 19.10.2017.
 */
public class Book implements Serializable{
    private int id;
    private String title;
    private String nameAuthor;
    private int date;
    private int countPage;
    private int countSample;
    private int status;
    private BigDecimal price;


    public Book(){}

    public Book(String title, String nameAuthor, int date, int countPage, int countSample, int status, BigDecimal price) {
        this.title = title;
        this.nameAuthor = nameAuthor;
        this.date = date;
        this.countPage = countPage;
        this.countSample = countSample;
        this.status = status;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNameAuthor() {
        return nameAuthor;
    }

    public void setNameAuthor(String nameAuthor) {
        this.nameAuthor = nameAuthor;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public int getCountPage() {
        return countPage;
    }

    public void setCountPage(int countPage) {
        this.countPage = countPage;
    }

    public int getCountSample() {
        return countSample;
    }

    public void setCountSample(int countSample) {
        this.countSample = countSample;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (id != book.id) return false;
        if (date != book.date) return false;
        if (countPage != book.countPage) return false;
        if (countSample != book.countSample) return false;
        if (status != book.status) return false;
        if (title != null ? !title.equals(book.title) : book.title != null) return false;
        if (nameAuthor != null ? !nameAuthor.equals(book.nameAuthor) : book.nameAuthor != null) return false;
        return price != null ? price.equals(book.price) : book.price == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (nameAuthor != null ? nameAuthor.hashCode() : 0);
        result = 31 * result + date;
        result = 31 * result + countPage;
        result = 31 * result + countSample;
        result = 31 * result + status;
        result = 31 * result + (price != null ? price.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", nameAuthor='" + nameAuthor + '\'' +
                ", date=" + date +
                ", countPage=" + countPage +
                ", countSample=" + countSample +
                ", status=" + status +
                ", price=" + price +
                '}';
    }
}
