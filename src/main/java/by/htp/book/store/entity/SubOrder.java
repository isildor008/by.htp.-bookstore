package by.htp.book.store.entity;

import java.io.Serializable;

/**
 * Created by Dmitry on 03.11.2017.
 */
public class SubOrder implements Serializable {
    private int id;
    private int bookId;
    private int orderId;

    public SubOrder(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SubOrder subOrder = (SubOrder) o;

        if (id != subOrder.id) return false;
        if (bookId != subOrder.bookId) return false;
        return orderId == subOrder.orderId;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + bookId;
        result = 31 * result + orderId;
        return result;
    }

    @Override
    public String toString() {
        return "SubOrder{" +
                "id=" + id +
                ", bookId=" + bookId +
                ", orderId=" + orderId +
                '}';
    }
}
