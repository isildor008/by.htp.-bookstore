package by.htp.book.store.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * Created by Dmitry on 23.10.2017.
 */
public class FilterForEncoding implements Filter {
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        servletRequest.setCharacterEncoding("UTF-8");
        filterChain.doFilter(servletRequest, servletResponse);
    }

    public void destroy() {

    }
}
