package by.htp.book.store.model;

import java.math.BigDecimal;

/**
 * Created by Dmitry on 31.10.2017.
 */
public class BookModel {

    private int id;
    private String title;
    private BigDecimal price;

    public BookModel(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BookModel bookModel = (BookModel) o;

        if (id != bookModel.id) return false;
        if (title != null ? !title.equals(bookModel.title) : bookModel.title != null) return false;
        return price != null ? price.equals(bookModel.price) : bookModel.price == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "BookModel{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", price=" + price +
                '}';
    }
}
