package by.htp.book.store.model;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Created by Dmitry on 30.10.2017.
 */
public class OrderModel {
    private int orderId;
    private int userId;
    private Timestamp date;
    private String loginUser;
    private BigDecimal price;

    public OrderModel(){}

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(String loginUser) {
        this.loginUser = loginUser;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "OrderModel{" +
                "orderId=" + orderId +
                ", userId=" + userId +
                ", date=" + date +
                ", loginUser='" + loginUser + '\'' +
                ", price=" + price +
                '}';
    }
}
