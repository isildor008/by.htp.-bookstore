package by.htp.book.store.service;

import by.htp.book.store.exception.DaoException;

import java.util.List;

/**
 * Created by Dmitry on 16.11.2017.
 */
public interface OrderService {

    void addOrderByUser(int userId, List<Integer> list) throws DaoException;
}
