package by.htp.book.store.service;

import by.htp.book.store.entity.User;
import by.htp.book.store.exception.DaoException;

/**
 * Created by Dmitry on 16.11.2017.
 */
public interface UserService {
    User getUserByLoinAndPass(String login, String pass) throws DaoException;
}
