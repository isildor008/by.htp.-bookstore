package by.htp.book.store.service.impl;

import by.htp.book.store.dao.OrderDao;
import by.htp.book.store.dao.SubOrderDao;
import by.htp.book.store.dao.impl.OrderDaoImpl;
import by.htp.book.store.dao.impl.SubOrderDaoImpl;
import by.htp.book.store.exception.DaoException;
import by.htp.book.store.service.OrderService;

import java.sql.Timestamp;
import java.util.List;

import static by.htp.book.store.util.SQLConstant.STATUS_ORDER_IN_PROGRESS;

/**
 * Created by Dmitry on 31.10.2017.
 */
public class OderServiceImpl implements OrderService {

    public void addOrderByUser(int userId, List<Integer> list) throws DaoException {
        SubOrderDao subOrderDao=new SubOrderDaoImpl();
        OrderDao orderDao=new OrderDaoImpl();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        orderDao.saveOrder(userId, timestamp, STATUS_ORDER_IN_PROGRESS);
        for(int i=0;i<list.size();i++){
            subOrderDao.saveSubOrder(userId,list.get(i));
        }

    }
}
