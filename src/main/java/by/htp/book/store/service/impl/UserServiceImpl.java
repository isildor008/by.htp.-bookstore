package by.htp.book.store.service.impl;
import by.htp.book.store.dao.UserDao;
import by.htp.book.store.dao.impl.UserDaoImpl;
import by.htp.book.store.entity.User;
import by.htp.book.store.exception.DaoException;
import by.htp.book.store.service.UserService;
import by.htp.book.store.util.ConnectionPool;

/**
 * Created by Dmitry on 03.11.2017.
 */
public class UserServiceImpl implements UserService{

    {
        dao=new UserDaoImpl();
    }
    private UserDao dao;

    public User getUserByLoinAndPass(String login, String pass) throws DaoException {
        ConnectionPool pool = ConnectionPool.getInstance();
        pool.init();
        return dao.getUserByLoginAndPass(login, pass);
    }
}
