package by.htp.book.store.util;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import static by.htp.book.store.util.DBConstant.*;


/**
 * Created by Dmitry on 19.10.2017.
 */
public final class ConnectionPool implements Closeable {

    private static final ResourceBundle bundle = ResourceBundle.getBundle(RESOURCES_FILE_NAME);
    private static final ConnectionPool instance = new ConnectionPool();
    private BlockingQueue<Connection> freeConnection;
    private BlockingQueue<Connection> busyConnection;

    private int poolSize;
    private String driver;
    private String user;
    private String password;
    private String url;


    private ConnectionPool() {
        this.driver = bundle.getString(DB_DRIVER);
        this.user = bundle.getString(DB_LOGIN);
        this.password = bundle.getString(DB_PASSWORD);
        this.url = bundle.getString(DB_URL);
        this.poolSize = POOL_SIZE;

    }

    public void init()  {

        freeConnection = new ArrayBlockingQueue(poolSize);

        busyConnection = new ArrayBlockingQueue(poolSize);

        try {
            Class.forName(driver);
            for (int i = 0; i < poolSize; i++) {
                freeConnection.add(DriverManager.getConnection(url, user, password));
            }
        } catch (ClassNotFoundException e) {


        } catch (SQLException e) {

        }

    }

    public Connection take()  {
        Connection connection=null;
        try {
            connection = freeConnection.take();
            busyConnection.put(connection);
        } catch (InterruptedException e) {

        }
        return connection;
    }

    public void free(Connection connection) throws InterruptedException {
        if (connection == null) {
        }
        busyConnection.remove(connection);
        freeConnection.put(connection);
    }

    public static ConnectionPool getInstance() {
        return instance;
    }


    public void close() throws IOException {
        List<Connection> listConnection = new ArrayList<Connection>();
        listConnection.addAll(this.busyConnection);
        listConnection.addAll(this.freeConnection);

        for (Connection connection : listConnection) {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {


            }
        }
    }

    public void closeConnection(Connection con, PreparedStatement preSt) {
        if (con != null) {
            try {
                free(con);
            } catch (Exception e) {

            }
        }

        if (preSt != null) {
            try {
                preSt.close();
            } catch (SQLException e) {

            }
        }
    }


}
