package by.htp.book.store.util;

/**
 * Created by Dmitry on 19.10.2017.
 */
public final class DBConstant {
    public static final String RESOURCES_FILE_NAME="dbconfig";
    public static final int POOL_SIZE=10;
    public static final String DB_DRIVER = "db.driver";
    public static final String DB_LOGIN = "db.login";
    public static final String DB_PASSWORD = "db.password";
    public static final String DB_URL = "db.url";
    public static final String DB_POOLSIZE = "db.poolsize";
}
