package by.htp.book.store.util;

/**
 * Created by Dmitry on 04.11.2017.
 */
public final class ExceptionName {

    private ExceptionName(){}

    //BookDaoException
    public static final String EXCEPTION_ADD_NEW_BOOK_IN_DB="Problem is saving new book in data base.";
    public static final String EXCEPTION_EDIT_BOOK="Problem is updating book in data base.";
    public static final String EXCEPTION_GET_ALL_BOOK="Problem with execute query all book from data base.";
    public static final String EXCETPION_DELETE_BOOK="Problem id deleting book in data base.";
    public static final String EXCETPION_GET_BOOK_BY_STATUS="Problem with execute query book list from data base.";
    public static final String EXCETPION_GET_SORTED_LIST_BOOK="Problem with execute query sorted list by book.";
    public static final String EXCEPTION_GET_GET_BOOK_BY_COUNT_PAGE="Problem with execute query book by count page";

    //OrderDaoImpl
    public static final String EXCEPTION_SAVE_ORDER="Problem is saving date in table Order.";
    public static final String EXCEPTION_GET_ALL_ORDER_MODEL="Problem with execute query order model from data base.";
    public static final String EXCEPTION_CREATE_ORDER_MODEL="Problem with crate order model.";
    public static final String EXCEPTION_GET_ALL_BOOK_BY_ORDER="Problem with execute query all book by order from data base.";

    //SubOrderDaoImpl
    public static final String EXCEPTION_ADD_NEW_SUB_ORDER="Problem is saving sub order in data base.";

    //UserDaoImpl
    public static final String EXCEPTION_ADD_NEW_USER="Problem is saving user in data base.";
    public static final String EXCEPTION_GET_USER_LIST ="Problem with execute query all users from data base.";
    public static final String EXCEPTION_DELETE_USER="Problem is deleting user from data base.";
    public static final String EXCEPTION_GET_USERS_BY_STATUS="Problem with execute query users by status from data base.";
    public static final String EXCEPTION_GET_USERS_BY_SORTING="Problem with execute query users with sorting";
    public static final String EXCEPTION_GET_USER_BY_ID="Problem with execute query user by id.";
    public static final String EXCEPTION_EDIT_USER="Problem is updating user in data base.";



    public static final String EXECUTE_QUERY_ERROR = "An error was occurred while executing the query to the database.";
    public static final String DATABASE_ACCESS_ERROR = "Database is not available.";
    public static final String CLASS_FOR_NAME_ERROR = "Description according to Class.forName() was not found.";
    public static final String STATEMENT_ERROR = "Statement object is null.";
    public static final String RESULTSET_ERROR = "ResultSet object is empty.";
    public static final String CONNECTION_ERROR = "Connection object is null.";

}
