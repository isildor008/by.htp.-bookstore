package by.htp.book.store.util;

/**
 * Created by Dmitry on 20.10.2017.
 */
public final class  SQLConstant {

    private SQLConstant(){}
    //Book SQL Constant
    public static final String INSERT_BOOK = "INSERT INTO `book_store`.`book` (`title`, `name_author`, `date`, `count_page`, `count_sample`,`status`, `price`) VALUES (?,?,?,?,?,?,?)";
    public static final String GET_ALL_BOOK="SELECT * FROM `book_store`.`book`";
    public static final String UPDATE_BOOK="UPDATE `book_store`.`book` SET `title`=?, `name_author`=?, `date`=?, `count_page`=? ,`count_sample`=?,`status`=?, `price`=?  WHERE  `id`=?;";
    public static final String DELETE_BOOK="UPDATE `book_store`.`book` SET `status`=? WHERE  `id`=?;";
    public static final String GET_BOOK_LIST_BY_STATUS="SELECT `id`, `title`, `name_author`, `date`, `count_page`, `count_sample`, `status`, `price` FROM `book_store`.`book` WHERE  `status`=?";
    public static final String SORTING_BOOK_BY_PRICE="SELECT * FROM `book_store`.`book` ORDER BY `price`";
    public static final String SORTING_BOOK_BY_DATE="SELECT * FROM `book_store`.`book` ORDER BY `date`";
    // for test
    public static final String DELETE_BOOK_BY_COUNT_PAGE="DELETE FROM `book_store`.`book` WHERE  `count_page`=1178;";
    public static final String GET_BOOK_BY_COUNT_PAGE="SELECT*FROM `book_store`.`book` WHERE count_page=1178";


    //User SQL Constant

    public static final String INSERT_USER="INSERT INTO `book_store`.`user` (`login`, `pass`, `first_name`, `last_name`, `email`, `status`, `role`) VALUES (?,?, ?, ?, ?, ?,?);";
    public static final String GET_ALL_USER="SELECT * FROM `book_store`.`user`";
    public static final String UPDATE_USER="UPDATE `book_store`.`user` SET `login`=?, `pass`=?, `first_name`=?,`last_name`=?, `email`=?, `status` =?, `role`=? WHERE  `id`=?;";
    public static final String DELETE_USER="UPDATE `book_store`.`user` SET `status`=? WHERE  `id`=?;";
    public static final String GET_USER_LIST_BY_STATUS="SELECT * FROM `book_store`.`user` WHERE  `status`=?";
    public static final String GET_USER_BY_ID="SELECT* FROM `book_store`.`user` where `id`=?";
    public static final String SORTING_USER_LIST_BY_ID="SELECT * FROM `book_store`.`user` ORDER BY `id`";
    public static final String GET_USER_BY_LOGIN_AND_PASS="SELECT * FROM `book_store`.`user`  WHERE `login`=? AND `pass`=?";
    //for test
    public static final String DELETE_USER_BY_STATUS="DELETE FROM `book_store`.`user` WHERE `status`=?";

    //Order SQL Constant
    public static final String GET_ALL_BOOK_FOR_ORDER="SELECT `book_id`,`title`, `price` from book JOIN sub_order sub_order ON (sub_order.book_id=book.id) where sub_order.order_id=? ";
    public static final String GET_ALL_ORDER="SELECT * FROM `book_store`.`order` ORDER BY `date` DESC";
    public static final String GET_ALL_ORDER_TO_MODEL_WHITSOUT_SUM="SELECT orders.id, orders.user_id,  orders.date, user.login  FROM orders JOIN user ON user.id = orders.user_id order by `date` DESC";
    public static final String GET_SUM_BY_ORDER="SELECT SUM(price) from `book_store`.`book` JOIN `book_store`.`sub_order`ON (`sub_order`.`book_id`=`book`.`id`) where `sub_order`.`order_id`=?";
    public static final String SAVE_ORDER="INSERT INTO `book_store`.`orders` (`user_id`, `date`, `status`) VALUES (?, ?, ?);";
    public static final String GET_ORDER_BY_STATUS="SELECT * FROM `book_store`.`orders` WHERE `status`=?";
    public static final String DELETE_TESTED_ORDER="DELETE FROM `book_store`.`orders` WHERE `status`=?";


    //Sub order Constant
    public static final String SAVE_SUB_ORDER="INSERT INTO `book_store`.`sub_order` (`book_id`, `order_id`) VALUES (?, ?);";
    public static final String GET_SUB_ORDER_BY_ID="SELECT* FROM  book_store`.`sub_order` WHERE `id`=? ";
    public static final String GET_SUB_ORDER_LIST_BY_ORDER_ID="SELECT* FROM  `book_store`.`sub_order` WHERE `order_id`=? ";
    //for test
    public static final String DELETE_SUB_ORDER_BY_BOOK_ID="DELETE FROM `book_store`.`sub_order` WHERE  `book_id`=?;";

    // Book additional constants
    public static final int NOT_AVAILABLE_BOOK=0;
    public static final int AVAILABLE_BOOK=1;



    // User additional constants
    public static final int INACTIV_USER=0;
    public static final int ACTIV_USER=1;



    // Order additional constants
    public static final int STATUS_ORDER_IN_PROGRESS=1;


    //sorting
    public static final String SORTING_ASCENDING="ASC";
    public static final String SORTING_DECREASE="DESC";




}
