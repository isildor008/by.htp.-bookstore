package by.htp.book.store.dao;

import by.htp.book.store.dao.impl.BookDaoImpl;
import by.htp.book.store.entity.Book;
import by.htp.book.store.exception.DaoException;
import by.htp.book.store.util.ConnectionPool;
import org.junit.Test;

import java.math.BigDecimal;

import static by.htp.book.store.util.SQLConstant.SORTING_BOOK_BY_DATE;
import static by.htp.book.store.util.SQLConstant.SORTING_DECREASE;


/**
 * Created by Dmitry on 04.11.2017.
 */
public class BookDaoImplTest {

    Book book = new Book("Lord of the ring", "J. R. R. Tolkien", 1954, 1178, 10, 1, new BigDecimal(115));
    BookDao bookDao = new BookDaoImpl();

    @Test
    public void addNewBookTest() {
        ConnectionPool pool = ConnectionPool.getInstance();
        pool.init();
        try {
            bookDao.addNewBook("Lord of the ring", "J. R. R. Tolkien", 1954, 1178, 10, 1, new BigDecimal(115));
            System.out.println("Save result test"+bookDao.getBookByCountPage(book));
            bookDao.deleteTestedBook();
        } catch (DaoException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void editBookTest() throws DaoException {
        ConnectionPool pool = ConnectionPool.getInstance();
        pool.init();
        Book bo=bookDao.getBookByCountPage(book);
        System.out.println("id new book= "+bo.getId());
        try {
            bookDao.editBook("1", "Valera", 1954, 1178, 1, 1, new BigDecimal(115),   bo.getId());
            System.out.println("Edit result test"+bookDao.getBookByCountPage(book));
            bookDao.deleteTestedBook();
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getBookListTest(){
        ConnectionPool pool = ConnectionPool.getInstance();
        pool.init();
        try {
            System.out.println("List book from db test: "+bookDao.getBookList());
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void deleteTest(){
        ConnectionPool pool = ConnectionPool.getInstance();
        pool.init();
        try {
            bookDao.addNewBook("Lord of the ring", "J. R. R. Tolkien", 1954, 1178, 10, 8, new BigDecimal(115));
            Book bo=bookDao.getBookByCountPage(book);
            bookDao.delete(bo.getId());
            System.out.println("Delete result test"+bookDao.getBookByCountPage(book));
            bookDao.deleteTestedBook();
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getBookListByStatusTest(){
        ConnectionPool pool = ConnectionPool.getInstance();
        pool.init();
        try {
            System.out.println("List by status 1: "+bookDao.getBookListByStatus(1));
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getSortedListByDateBooksTest(){
        ConnectionPool pool = ConnectionPool.getInstance();
        pool.init();
        try {
            System.out.println("Sorted list by date"+bookDao.getSortedListBooks(SORTING_BOOK_BY_DATE,SORTING_DECREASE));
        } catch (DaoException e) {
            e.printStackTrace();
        }

    }



}
