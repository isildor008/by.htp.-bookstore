package by.htp.book.store.dao;

import by.htp.book.store.dao.impl.OrderDaoImpl;
import by.htp.book.store.exception.DaoException;
import by.htp.book.store.util.ConnectionPool;
import org.junit.Test;

import java.sql.Timestamp;

/**
 * Created by Dmitry on 04.11.2017.
 */
public class OrderDaoImplTest {

    Timestamp timestamp = new Timestamp(System.currentTimeMillis());

    OrderDao orderDao = new OrderDaoImpl();
    @Test
    public void saveOrderTest() {
        ConnectionPool pool = ConnectionPool.getInstance();
        pool.init();

        try {
            orderDao.saveOrder(1, timestamp, 8);
            System.out.println(orderDao.getByStatus(8));
            orderDao.deleteTestedOrder();
        } catch (DaoException e) {
            e.printStackTrace();
        }

    }
}
