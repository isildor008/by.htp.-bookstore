package by.htp.book.store.dao;

import by.htp.book.store.dao.impl.OrderModelDaoImpl;
import by.htp.book.store.exception.DaoException;
import by.htp.book.store.util.ConnectionPool;
import org.junit.Test;

/**
 * Created by Dmitry on 04.11.2017.
 */
public class OrderModelDaoImplTest {

    OrderModelDao orderModelDao=new OrderModelDaoImpl();

    @Test
    public void getAllOrderModelTest(){
        ConnectionPool pool = ConnectionPool.getInstance();
        pool.init();
        try {
           System.out.println("result get all order model: "+ orderModelDao.getAllOrderModel());
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getAllBookByOrderTest(){
        ConnectionPool pool = ConnectionPool.getInstance();
        pool.init();
        try {
            System.out.println("result get list all book by order"+orderModelDao.getAllBookByOrder(1));
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }
}
