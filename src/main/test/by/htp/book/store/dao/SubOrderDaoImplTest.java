package by.htp.book.store.dao;

import by.htp.book.store.dao.impl.SubOrderDaoImpl;
import by.htp.book.store.exception.DaoException;
import by.htp.book.store.util.ConnectionPool;
import org.junit.Test;

/**
 * Created by Dmitry on 04.11.2017.
 */
public class SubOrderDaoImplTest {

    SubOrderDao subOrderDao=new SubOrderDaoImpl();

    @Test
    public void saveSubOrderTest(){
        ConnectionPool pool = ConnectionPool.getInstance();
        pool.init();
        try {
            subOrderDao.saveSubOrder(1999,1999);
            System.out.println("Saved sub order: "+subOrderDao.getListByOrderId(1999));
            subOrderDao.deleteSubOrderByBookId(1999);

        } catch (DaoException e) {
            e.printStackTrace();
        }
    }
}
