package by.htp.book.store.dao;

import by.htp.book.store.dao.impl.UserDaoImpl;
import by.htp.book.store.exception.DaoException;
import by.htp.book.store.util.ConnectionPool;
import org.junit.Test;

import static by.htp.book.store.util.SQLConstant.SORTING_ASCENDING;
import static by.htp.book.store.util.SQLConstant.SORTING_USER_LIST_BY_ID;

/**
 * Created by Dmitry on 04.11.2017.
 */
public class UserDaoImplTest {


    UserDao userDao = new UserDaoImpl();

    @Test
    public void addNewUserTest() {
        ConnectionPool pool = ConnectionPool.getInstance();
        pool.init();
        try {
            userDao.addNewUser("Vasja195", "qwerty", "Vasja", "Petrow", "3hlorSilan@gmail.ru", 9, 9);
            System.out.println(userDao.getUserListByStatus(9));
            userDao.deleteUserByStatus(9);
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void editUserTest() {
        ConnectionPool pool = ConnectionPool.getInstance();
        pool.init();
        try {
            userDao.addNewUser("Vasja195", "qwerty", "Vasja", "Petrow", "3hlorSilan@gmail.ru", 9, 9);
            userDao.editUser("vitalik", "qwerty", "Vasja", "Petrow", "lexa21@mail.ru", 9, 9, userDao.getUserListByStatus(9).get(0).getId());
            System.out.println("updating user: " + userDao.getUserListByStatus(9));
            userDao.deleteUserByStatus(9);
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getUserListTest() {
        ConnectionPool pool = ConnectionPool.getInstance();
        pool.init();
        try {
            System.out.println("user list test: " + userDao.getUserList());
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void deleteTest() {
        ConnectionPool pool = ConnectionPool.getInstance();
        pool.init();
        try {
            userDao.addNewUser("Vasja195", "qwerty", "Vasja", "Petrow", "3hlorSilan@gmail.ru", 9, 9);
            int id = userDao.getUserListByStatus(9).get(0).getId();
            userDao.delete(id);
            System.out.println("updating status=0" + userDao.getUserById(id));
            userDao.editUser("vitalik", "qwerty", "Vasja", "Petrow", "lexa21@mail.ru", 9, 9, id);
            userDao.deleteUserByStatus(9);
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getSortedListUsersTest() {
        ConnectionPool pool = ConnectionPool.getInstance();
        pool.init();
        try {
            System.out.println("Sorted list by id" + userDao.getSortedListUsers(SORTING_USER_LIST_BY_ID, SORTING_ASCENDING));
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }
}
