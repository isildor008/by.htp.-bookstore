<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <%@include file="/WEB-INF/static/common_css.jsp" %>
    <title></title>
</head>
<body>
<%@include file="/WEB-INF/tile/adminTile/headerAdmin.jsp" %>
<%@include file="/WEB-INF/tile/adminTile/adminTile.jsp" %>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<%@include file="/WEB-INF/static/common_js.jsp" %>
</body>
</html>
