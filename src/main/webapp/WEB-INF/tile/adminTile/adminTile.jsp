<%--
  Created by IntelliJ IDEA.
  User: Dmitry
  Date: 15.11.2017
  Time: 15:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<section class="admin-home-page-table">
    <div class="user-list-table">
        <table class="full-user-list-table">
            <tr>
                <th>ID</th>
                <th>User Name</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Role</th>
                <th>Status</th>
                <th>Actions</th>

            </tr>
            <tr>
                <td> 001</td>
                <td>Admin </td>
                <td>Patseev </td>
                <td>Dzmitry </td>
                <td>isildor.008@mail.ru </td>
                <td>Admin </td>
                <td> </td>
                <td> </td>

            </tr>
            <tr>
                <td>002 </td>
                <td>User </td>
                <td>Ivan </td>
                <td>Ivanov</td>
                <td>ivanov@gmail.com </td>
                <td>user </td>
                <td>
        <span class="toggle-bg">
 <input type="radio" name="toggle" value="off">
 <input type="radio" name="toggle" value="on">
 <span class="switch"></span>
</span>
                </td>
                <td><a href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 32 32">
                    <title>pencil2</title>
                    <path d="M12 20l4-2 14-14-2-2-14 14-2 4zM9.041 27.097c-0.989-2.085-2.052-3.149-4.137-4.137l3.097-8.525 4-2.435 12-12h-6l-12 12-6 20 20-6 12-12v-6l-12 12-2.435 4z"></path>
                </svg></a>
                    <a href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 32 32">
                        <title>bin</title>
                        <path d="M4 10v20c0 1.1 0.9 2 2 2h18c1.1 0 2-0.9 2-2v-20h-22zM10 28h-2v-14h2v14zM14 28h-2v-14h2v14zM18 28h-2v-14h2v14zM22 28h-2v-14h2v14z"></path>
                        <path d="M26.5 4h-6.5v-2.5c0-0.825-0.675-1.5-1.5-1.5h-7c-0.825 0-1.5 0.675-1.5 1.5v2.5h-6.5c-0.825 0-1.5 0.675-1.5 1.5v2.5h26v-2.5c0-0.825-0.675-1.5-1.5-1.5zM18 4h-6v-1.975h6v1.975z"></path>
                    </svg></a> </td>
            </tr>
        </table>
    </div>


    </div>
</section>