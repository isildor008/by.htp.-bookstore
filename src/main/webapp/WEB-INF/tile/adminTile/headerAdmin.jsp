<%--
  Created by IntelliJ IDEA.
  User: Dmitry
  Date: 15.11.2017
  Time: 15:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<section class="navigation-menu">
    <nav class="navbar  fixed-top navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Book Store</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link active" href="#">Main Page<span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link" href="#">Catalog</a>
                <a class="nav-item nav-link" href="#">Add Book</a>
                <a class="nav-item nav-link" href="#">List Book</a>
                <a class="nav-item nav-link" href="#">Add User</a>
                <a class="nav-item nav-link" href="#">List User</a>

            </div>
        </div>
    </nav>
</section>