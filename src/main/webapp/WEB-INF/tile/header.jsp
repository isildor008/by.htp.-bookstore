<%--
  Created by IntelliJ IDEA.
  User: Dmitry
  Date: 15.11.2017
  Time: 15:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<section class="navigation-menu">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Book Store</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link active" href="#">Main Page<span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link" href="#">Catalog</a>
                <a class="nav-item nav-link" href="MainServlet?action=login">Log in</a>

                 <%--<c:out value="${firstName}"/>--%>
                    <%--<li><a href="MainServlet?action=logout"><i class="fa fa-lock"></i> Logout</a></li>--%>
                <c:choose>
                <c:when test="${firstName!=null}">
                    <c:out value="${firstName}"/>
                    <li><a href="MainServlet?action=logout"><i class="fa fa-lock"></i> Logout</a></li>
                </c:when>
                </c:choose>
            </div>
        </div>
    </nav>
</section>
