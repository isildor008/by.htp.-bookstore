<%--
  Created by IntelliJ IDEA.
  User: Dmitry
  Date: 15.11.2017
  Time: 15:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<section class="main-page-slider">
    <div class="slider-container">

        <div class="slider-container">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="resources/img/slider-1.jpg" height="500px"; width="700px" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="resources/img/slider-2.jpg"  height="500px" width="700px" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="resources/img/slider-3.jpg"  height="500px" width="700px" alt="Third slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="resources/img/slider-4.png"  height="500px" width="700px" alt="Third slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>

</section>

<section class="book-cards">
    <div class="container">
        <div class="row justify-content-between">
            <div class=" col-xs-12 col-xm-6 col-md-4 ">
                <div class="book-card-main-page">
                    <div class="card" style="width: 20rem;">
                        <img class="card-img-top" src="resources/img/book-1.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h4 class="card-title">Card title</h4>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class=" col-xs-12 col-xm-6 col-md-4 ">
                <div class="book-card-main-page">
                    <div class="card" style="width: 20rem;">
                        <img class="card-img-top" src="resources/img/book-2.png" alt="Card image cap">
                        <div class="card-body">
                            <h4 class="card-title">Card title</h4>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class=" col-xs-12 col-xm-6 col-md-4 ">
                <div class="book-card-main-page">
                    <div class="card" style="width: 20rem;">
                        <img class="card-img-top" src="resources/img/book-3.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h4 class="card-title">Card title</h4>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class=" col-xs-12 col-xm-6 col-md-4 ">
                <div class="book-card-main-page">
                    <div class="card" style="width: 20rem;">
                        <img class="card-img-top" src="resources/img/book-4.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h4 class="card-title">Card title</h4>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class=" col-xs-12 col-xm-6 col-md-4 ">
                <div class="book-card-main-page">
                    <div class="card" style="width: 20rem;">
                        <img class="card-img-top" src="resources/img/book-4.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h4 class="card-title">Card title</h4>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class=" col-xs-12 col-xm-6 col-md-4 ">
                <div class="book-card-main-page">
                    <div class="card" style="width: 20rem;">
                        <img class="card-img-top" src="resources/img/book-4.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h4 class="card-title">Card title</h4>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>